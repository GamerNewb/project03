﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Waypoint))]
public class FacingDrawer : PropertyDrawer {

    Waypoint waypointScript;
    float extraHeight = 55f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {

        EditorGUI.BeginProperty(position, label, property);

        //Rects for placement
        Rect typeRect = new Rect(position.x, position.y, position.width, 17f);

        //Serialized Properties
        SerializedProperty waypointType = property.FindPropertyRelative("FacingType");
        SerializedProperty lookTarget = property.FindPropertyRelative("lookTarget");

        //Displaying Properties
        EditorGUI.PropertyField(typeRect, waypointType, GUIContent.none);

        


        switch ((FacingType)waypointType.enumValueIndex)
        {
            case FacingType.FORCED_LOOK:

                break;
            case FacingType.FREE_MOVEMENT:

                break;
            default:
                break;
        }

        EditorGUI.EndProperty();
    }
}
