﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(Engine))]
public class EngineEditor : Editor
{

    Engine engineScript;

    void Awake()
    {
        engineScript = (Engine)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        SerializedProperty controller = serializedObject.FindProperty("waypoints");

        EditorGUILayout.PropertyField(controller);

        if (controller.isExpanded)
        {
            EditorGUILayout.PropertyField(controller.FindPropertyRelative("Array.size"));

            EditorGUI.indentLevel++;

            for (int i = 0; i < controller.arraySize; i++)
            {
                EditorGUILayout.PropertyField(controller.GetArrayElementAtIndex(i));
            }
            EditorGUI.indentLevel--;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
