﻿using UnityEngine;
using System.Collections;


    public enum Type
    {
        MOVEMENT = 1,
        FACING,
        EFFECTS
    }

    public enum MoveType
    {
        STRAIGHT_LINE = 1,
        BEZIER_CURVE,
        WAIT,
        LOOK_RETURN,
        LOOK_CHAIN
    }

    public enum FacingType
    {
        FREE_MOVEMENT = 1,
        FORCED_LOOK
    }

    public enum EffectType
    {
        CAMERA_SHAKE = 1,
        SPLATTER,
        FADE
    }

    [System.Serializable]
    public class Waypoint : MonoBehaviour
    {
        public int speed;
        public Transform startPoint;
        public Transform endPoint;
        public Transform bezierCurve;
        public Transform lookTarget;
        public Transform[] chainTransforms;
    }

